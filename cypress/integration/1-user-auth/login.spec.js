/* eslint-disable jest/valid-expect-in-promise */
/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

describe('Users are required to be logged in', () => {
  it('Visits the home page', () => {
    cy.visit(Cypress.env('baseUrl'))
      .location('pathname')
      .should('eq', '/login')
      .visit(`${Cypress.env('baseUrl')}/bookings/upcoming`)
      .location('pathname')
      .should('eq', '/login')
      .get('input[name="email"]')
      .type('foo@bar.com')
      .get('input[name="password"]')
      .type('foobar')
      .get('button[type="submit"]')
      .click()
      .location('pathname')
      .should('eq', '/bookings/upcoming')
      .window()
      .its('store')
      .invoke('getState')
      .then((state) => {
        expect(state.auth.isLoggedIn).to.be.a('boolean').and.equal(true)
      })
      .visit(Cypress.env('baseUrl'))
      .location('pathname')
      .should('eq', '/bookings/upcoming')
  })
})
