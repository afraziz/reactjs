/// <reference types="cypress" />
describe('Users should be able to reset their password', () => {
  it('Visits the forgot password page', () => {
    cy.visit(`${Cypress.env('baseUrl')}/forgot-password`)
      .location('pathname')
      .should('eq', '/forgot-password')
      .get('input[name="email"]')
      .type('foo@gmail.com')
      .get('button[type="submit"]')
      .click()
      .get('.modal-inner-content h3')
      .invoke('text')
      .should('eq', 'Your password reset link is on it’s way.')
  })
})
