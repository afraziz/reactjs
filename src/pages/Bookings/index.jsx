import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link, Outlet } from 'react-router-dom'

import { logout } from '@feat/auth/authSlice'
import { HeadingLarge, ContentWrapper } from 'styles/general-styles'
// import { Button } from '~/Common/Button';
// import { theme } from '../../theme';
import { ButtonOutline } from '~/Button/ButtonOutline'

const Bookings = ({ logout }) => {
  // const location = useLocation()

  // const type = () => {
  //   const paths = location?.pathname?.split('/')
  //   return paths?.[paths.length - 1]
  // }

  return (
    <ContentWrapper>
      <HeadingLarge style={{ marginBottom: 30 }}>Bookings</HeadingLarge>

      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Link to="/bookings/upcoming">
          {/* <Button
                        text="Upcoming"
                        bgColor={type() === 'upcoming' ? theme.colors.blue : theme.colors.blueGreyLight}
                    /> */}
        </Link>
        <Link to="/bookings/past">
          {/* <Button
                        text="Past Bookings"
                        bgColor={type() === 'past' ? theme.colors.blue : theme.colors.blueGreyLight}
                    /> */}
        </Link>
        <ButtonOutline
          onClick={() => {
            logout()
          }}
        >
          Logout
        </ButtonOutline>
        {/* <Button onClick={() => {
                    logout();
                }} text="Logout" secondary type="button" /> */}
      </div>

      <Outlet />
    </ContentWrapper>
  )
}

Bookings.propTypes = {
  logout: PropTypes.func,
}

export default connect(null, { logout })(Bookings)
