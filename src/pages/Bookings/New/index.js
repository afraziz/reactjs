import PropTypes from 'prop-types'
import { useEffect, useState } from 'react'
import { connect } from 'react-redux'

import { setProgress } from '@feat/registration/registrationSlice'
// import AddTotalAmount from '~/Bookings/AddTotalAmount'
// import AddCattle from '~/Bookings/AddCattle'
// import CattleDetails from '~/Bookings/CattleDetails'
// import EnterAge from '~/Bookings/EnterAge'
// import LiveWeight from '~/Bookings/LiveWeight'
// import AddMedia from '~/Bookings/AddMedia'
// import AdditionalInfo from '~/Bookings/AdditionalInfo'
// import DeliveryDate from '~/Bookings/DeliveryDate'
// import AltDeliveryDate from '~/Bookings/AltDeliveryDate'
// import Breed from '~/Bookings/Breed'
// import ReviewBooking from '~/Bookings/ReviewBooking'
// import BookingSubmitted from '~/Bookings/BookingSubmitted'
// import AgentAccess from '~/Bookings/AgentAccess'
// import ProgressSteps from '~/Bookings/ProgressSteps'

const New = ({ bookings }) => {
  const [currentStep] = useState()
  // const [percent, setPercent] = useState(0);

  useEffect(() => {
    // switch (bookings.progressStep) {
    //   case 1:
    //     setCurrentStep(<AddTotalAmount steps={<ProgressSteps percent={0} />} />)
    //     break
    //   case 2:
    //     setCurrentStep(<AddCattle steps={<ProgressSteps percent={16} />} />)
    //     break
    //   case 3:
    //     setCurrentStep(<CattleDetails />)
    //     break
    //   case 4:
    //     setCurrentStep(<EnterAge />)
    //     break
    //   case 5:
    //     setCurrentStep(<LiveWeight />)
    //     break
    //   case 6:
    //     setCurrentStep(<AddMedia />)
    //     break
    //   case 7:
    //     setCurrentStep(<AdditionalInfo />)
    //     break
    //   case 8:
    //     setCurrentStep(<Breed />)
    //     break
    //   case 9:
    //     setCurrentStep(<DeliveryDate steps={<ProgressSteps percent={32} />} />)
    //     break
    //   case 10:
    //     setCurrentStep(
    //       <AltDeliveryDate steps={<ProgressSteps percent={48} />} />
    //     )
    //     break
    //   case 11:
    //     setCurrentStep(<ReviewBooking steps={<ProgressSteps percent={64} />} />)
    //     break
    //   case 12:
    //     setCurrentStep(<BookingSubmitted />)
    //     break
    //   case 13:
    //     setCurrentStep(<AgentAccess />)
    //     break
    //   default:
    //     setCurrentStep(<AddTotalAmount />)
    //     break
    // }
  }, [bookings.progressStep])

  return <>{currentStep}</>
}

New.propTypes = {
  bookings: PropTypes.shape({
    progressStep: PropTypes.any,
  }),
  setProgress: PropTypes.any,
}

const mapStateToProps = (state) => ({
  bookings: state.bookings,
})

export default connect(mapStateToProps, { setProgress })(New)

// export default New;
