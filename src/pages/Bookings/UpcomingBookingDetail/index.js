import { useState } from 'react'
import { Link } from 'react-router-dom'
import { Row } from 'react-bootstrap'

import {
  HeadingLarge,
  HeadingMedium,
  HeadingSmall,
  ContentWrapper,
  NoPaddingContainer,
} from 'styles/general-styles'
// import { UpcomingBookingsCol } from 'styles/bookings-styles'
// import { Button } from '~/Common/Button'

const UpcomingBookingDetail = () => {
  const [type] = useState('overview')

  // const DATA = [
  //   {
  //     id: '1',
  //     type: 'Incomplete',
  //     image: require('@img/upcomingBookings/upcoming_high_res.jpg'),
  //   },
  //   {
  //     id: '2',
  //     type: 'Pending',
  //     image: require('@img/upcomingBookings/upcoming_high_res.jpg'),
  //   },
  //   {
  //     id: '3',
  //     type: 'Incomplete',
  //     image: require('@img/upcomingBookings/upcoming_high_res.jpg'),
  //   },
  // ]

  // const bookingData = DATA.find((d) => d.id === bookingId)

  return (
    <>
      <ContentWrapper>
        <Link to="/bookings/upcoming">
          {/* <Button
            text="Back"
            width="100px"
            bgColor={theme.colors.blueLight}
            // marginBottom="30px"
            type="button"
          /> */}
        </Link>
        <HeadingLarge style={{ marginBottom: 30 }}>Your Booking</HeadingLarge>

        <NoPaddingContainer fluid>
          <Row>
            {/* <UpcomingBookingsCol className="col-12">
              <div className="content">
                <label className="type">{bookingData.type}</label>
              </div>
              <img src={bookingData.image.default} alt="upcoming bookings" />
            </UpcomingBookingsCol> */}
          </Row>
        </NoPaddingContainer>

        {/* <button onClick={() => setType('overview')}>Overview</button>
                <button onClick={() => setType('details')}>Details</button> */}

        <NoPaddingContainer fluid style={{ marginTop: 30, marginBottom: 30 }}>
          {/* <Button
            onClick={() => {
              setType('overview')
            }}
            text="Overview"
            bgColor={
              type === 'overview'
                ? theme.colors.blue
                : theme.colors.blueGreyLight
            }
            marginRight="20px"
          />
          <Button
            onClick={() => {
              setType('details')
            }}
            text="Details"
            bgColor={
              type === 'v' ? theme.colors.blue : theme.colors.blueGreyLight
            }
          /> */}
        </NoPaddingContainer>

        <NoPaddingContainer fluid>
          {type === 'overview' ? (
            <>
              <HeadingMedium>Ready to complete your booking?</HeadingMedium>
              <HeadingSmall>
                Once you’ve completed your booking in the app, you’ll see the
                details right here.
              </HeadingSmall>

              {/* <Button
                type="button"
                marginTop="100px"
                width="210px"
                text="Complete Kill Booking"
              />

              <Button
                type="button"
                marginTop="100px"
                width="210px"
                bgColor="transparent"
                color="red"
                text="Cancel Booking"
              /> */}
            </>
          ) : (
            <>
              <HeadingMedium>Details</HeadingMedium>

              {/* <Button
                type="button"
                marginTop="100px"
                width="210px"
                text="Complete Kill Booking"
              />

              <Button
                type="button"
                marginTop="100px"
                width="210px"
                bgColor="transparent"
                color="red"
                text="Cancel Booking"
              /> */}
            </>
          )}
        </NoPaddingContainer>
      </ContentWrapper>
    </>
  )
}

export default UpcomingBookingDetail
