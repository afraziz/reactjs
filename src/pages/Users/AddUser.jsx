import { useDispatch, useSelector } from "react-redux";

import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { Button} from 'components/Button'
import { InfoCard } from 'components/Cards/InfoCard'
import { Footer } from 'components/Footer'
import { TopNav } from 'components/TopNav'
import { SignoutIcon } from '@img/svg'
import { HeadingLarge } from 'styles/general-styles'
import { DashboardWrapper } from '../../layout/DashboardWrapper'
import { AccountDetailsStyled } from './styled'
import { userAdded } from "features/user/usersSlice";

export function AddUser() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [error, setError] = useState(null);

  const handleName = (e) => setName(e.target.value);
  const handleEmail = (e) => setEmail(e.target.value);

  const usersAmount = useSelector((state) => state.users.entities.length);

  const handleClick = () => {
    if (name && email) {
      dispatch(
        userAdded({
          id: usersAmount + 1,
          name,
          email,
        })
      );

      setError(null);
      navigate("/user-list");
    } else {
      setError("Fill in all fields");
    }

    setName("");
    setEmail("");
  };

  return (
    <AccountDetailsStyled>
    <DashboardWrapper>
      <TopNav />
      <div className="inner-content">
        <div className="d-flex justify-content-between">
          <HeadingLarge uppercase="true">Users List</HeadingLarge>
          <Button width="fit-content">
            <SignoutIcon className="mr-2" />
            {'  '}
            Sign Out
          </Button>
        </div>
        <InfoCard>
          <div className="card-header">
            <div>
              <h4>Add User</h4>
            </div>
          </div>
          <div className="form card-body">
                <div className="three columns">
                <label htmlFor="nameInput">Name</label>
                <div className="row">
                    <div className="col-sm-12">
                        <input
                            className="u-full-width"
                            type="text"
                            placeholder="test@mailbox.com"
                            id="nameInput"
                            onChange={handleName}
                            value={name}
                            />
                    </div>
                </div>
                <label htmlFor="emailInput">Email</label>
                <div className="row">
                    <div className="col-sm-12">
                        <input
                            className="u-full-width"
                            type="email"
                            placeholder="test@mailbox.com"
                            id="emailInput"
                            onChange={handleEmail}
                            value={email}
                        />
                    </div>
                </div>
              
                {error && error}
                <Button width="fit-content" onClick={handleClick} >
                    Add user
                </Button>
                </div>
          </div>
        </InfoCard>
      </div>
      <Footer />
    </DashboardWrapper>
  </AccountDetailsStyled>
  );
}