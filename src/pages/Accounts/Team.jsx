import { Avatar } from 'components/Avatar'
import { ButtonOutline } from 'components/Button'
import { Footer } from 'components/Footer'
import { TopNav } from 'components/TopNav'
import { ProfileIcon } from '@img/svg'
import { HeadingLarge } from 'styles/general-styles'
import { Link } from 'react-router-dom'
import { MdEdit } from 'react-icons/md'
import { BsPersonDashFill } from 'react-icons/bs'
import { IoAddCircle } from 'react-icons/io5'
import { DashboardWrapper } from '../../layout/DashboardWrapper'
import { TeamStyled } from './styled'

export const Team = () => {
  return (
    <TeamStyled>
      <DashboardWrapper>
        <TopNav title="Team" />
        <div className="inner-content">
          <div className="d-flex justify-content-between">
            <HeadingLarge uppercase="true">Team Managment</HeadingLarge>
          </div>

          <div className="card">
            <div className="card-header">
              <div>
                <h4>Your Team</h4>
                <span>Edit and manage your team</span>
              </div>
              <ButtonOutline width="fit-content">
                Add new user <IoAddCircle />{' '}
              </ButtonOutline>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-lg-3 col-md-4 col-6">
                  <div className="team-card">
                    <div className="tc-body">
                      <Avatar className={'avatar'} />
                      <span className="name">Tom Cook</span>
                      <span className="role">Account Owner</span>
                      <span className="type primary">Primary User</span>
                    </div>
                    <div className="tc-footer">
                      <Link to={'/'}>
                        <span className="icon">
                          <ProfileIcon />
                        </span>
                        View Account
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-4 col-6">
                  <div className="team-card">
                    <div className="tc-body">
                      <Avatar className={'avatar'} />
                      <span className="name">Tom Cook</span>
                      <span className="role">Account Owner</span>
                      <span className="type secondary">Primary User</span>
                    </div>
                    <div className="tc-footer">
                      <Link to={'/'} className="border-right">
                        <MdEdit className="icon" />
                        Edit
                      </Link>
                      <Link to={'/'}>
                        <BsPersonDashFill className="icon" />
                        Remove
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-4 col-6">
                  <div className="team-card">
                    <div className="tc-body">
                      <Avatar className={'avatar'} />
                      <span className="name">Tom Cook</span>
                      <span className="role">Account Owner</span>
                      <span className="type">Primary User</span>
                    </div>
                    <div className="tc-footer">
                      <Link to={'/'}>
                        <span className="icon">
                          <ProfileIcon />
                        </span>
                        View Account
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 col-md-4 col-6">
                  <div className="team-card">
                    <div className="tc-body">
                      <Avatar className={'avatar'} />
                      <span className="name">Tom Cook</span>
                      <span className="role">Account Owner</span>
                      <span className="type primary">Primary User</span>
                    </div>
                    <div className="tc-footer">
                      <Link to={'/'}>
                        <span className="icon">
                          <ProfileIcon />
                        </span>
                        View Account
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </DashboardWrapper>
    </TeamStyled>
  )
}
