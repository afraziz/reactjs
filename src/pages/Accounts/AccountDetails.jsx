import { Avatar } from 'components/Avatar'
import { useNavigate } from "react-router-dom";
import { connect } from 'react-redux'
import { Button, ButtonOutline } from 'components/Button'
import { InfoCard } from 'components/Cards/InfoCard'
import { Footer } from 'components/Footer'
import { Table } from 'components/Table'
import { TopNav } from 'components/TopNav'
import { SignoutIcon } from '@img/svg'
import { HeadingLarge } from 'styles/general-styles'
import { DashboardWrapper } from '../../layout/DashboardWrapper'
import { AccountDetailsStyled } from './styled'
import { logout } from '@feat/auth/authSlice'

const AccountDetails = ({logout}) => {

  const navigate = useNavigate();
  return (
    <AccountDetailsStyled>
      <DashboardWrapper>
        <TopNav />
        <div className="inner-content">
          <div className="d-flex justify-content-between">
            <HeadingLarge uppercase="true">Account</HeadingLarge>
            <Button width="fit-content" onClick={()=>{logout();navigate('/login');}}>
              <SignoutIcon className="mr-2" />
              {'  '}
              Sign Out
            </Button>
          </div>
          <InfoCard>
            <div className="card-header">
              <div>
                <h4>Your Details</h4>
                <span>Company and contact details</span>
              </div>
              <ButtonOutline width="fit-content">Edit Details</ButtonOutline>
            </div>
            <div className="card-body">
              <Table className=" table-striped">
                <tbody>
                  <tr>
                    <td>Full name</td>
                    <td>Tom Cook</td>
                  </tr>
                  <tr>
                    <td>Company</td>
                    <td>Beef Co.</td>
                  </tr>
                  <tr>
                    <td>Email Address</td>
                    <td>john.smith@email.com</td>
                  </tr>
                  <tr>
                    <td>Mobile Number</td>
                    <td>+91234567890</td>
                  </tr>
                </tbody>
              </Table>
            </div>
            <div className="card-footer">
              <p className="label">Profile Photo</p>
              <div className="d-flex align-items-center">
                <Avatar className="avatar" />
                <ButtonOutline background="#fff" width="fit-content">
                  Change
                </ButtonOutline>
              </div>
            </div>
          </InfoCard>

          {/* Billing detail card */}
          <InfoCard>
            <div className="card-header">
              <div>
                <h4>Billing Details</h4>
                <span>Billing Address & Payment Details</span>
              </div>
              <ButtonOutline width="fit-content">Edit Details</ButtonOutline>
            </div>
            <div className="card-body">
              <Table className="table-striped">
                <tbody>
                  <tr>
                    <td>Full name</td>
                    <td>Tom Cook</td>
                  </tr>
                  <tr>
                    <td>Company</td>
                    <td>XXXX Co.</td>
                  </tr>
                  <tr>
                    <td>Billing Address</td>
                    <td>22 Harvest Road, XXXX, XXXX XXXX 6XXX</td>
                  </tr>
                  <tr>
                    <td>Payment Details</td>
                    <td>
                      BSB: ***-456 <br />
                      Account: ***456
                    </td>
                  </tr>
                  <tr>
                    <td>ABN Number</td>
                    <td>ABN 64 117 222 985</td>
                  </tr>
                </tbody>
              </Table>
            </div>
          </InfoCard>

          <InfoCard>
            <div className="card-header border-bottom-0">
              <div>
                <h4>Update your password</h4>
                <span>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Recusandae voluptatibus corrupti atque repudiandae nam.
                </span>
              </div>
              <Button width="fit-content">Update Password</Button>
            </div>
          </InfoCard>
        </div>
        <Footer />
      </DashboardWrapper>
    </AccountDetailsStyled>
  )
}
export default connect(null, { logout })(AccountDetails)