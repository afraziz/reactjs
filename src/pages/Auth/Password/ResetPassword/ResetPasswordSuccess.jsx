import { useState } from 'react'
import { Button } from '~/Button'
import { HeadingLarge } from 'styles/general-styles'
import { IoCheckmarkOutline } from 'react-icons/io5'
import { BackgroundWrapper } from '~/BackgroundWrapper'
import { RegisterNavigation } from '~/Navigation'
import { PasswordResetSuccessStyled } from './styled'

const backgroundImage = require('@img/stay-in-touch.png')

export const ResetPasswordSuccess = () => {
  const [isModalOpen, setModalIsOpen] = useState(false)
  return (
    <PasswordResetSuccessStyled>
      <BackgroundWrapper className="bg-image" width="100%">
        <img
          src={backgroundImage}
          alt="background"
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            zIndex: 0,
          }}
        />
      </BackgroundWrapper>
      <RegisterNavigation
        title={'Sign in'}
        hideBack
        showClose
        isModalOpen={isModalOpen}
        setModalIsOpen={setModalIsOpen}
        className="navigation"
      />

      <div className="container">
        <div className="row d-flex justify-content-center">
          <div className="col-lg-5 col-md-6 col-12">
            <div className="info-card">
              <div className="success-logo d-flex">
                <div className="call-icon">
                  <IoCheckmarkOutline />
                </div>
              </div>
              <HeadingLarge uppercase>
                Your PASSWORD HAS BEEN CHANGED!
              </HeadingLarge>
              <p>
                Mauris massa ut morbi sodales id faucibus at tempus, amet.
                Semper ornare in id enim. Donec orci venenatis, leo molestie
                sagittis, dui. Vestibulum ut leo maecenas molestie.
              </p>
              <Button url="/login" data-test-id="goToLogin">
                Continue to login
              </Button>
            </div>
          </div>
        </div>
      </div>
    </PasswordResetSuccessStyled>
  )
}
