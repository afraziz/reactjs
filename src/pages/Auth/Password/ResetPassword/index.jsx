import { useState } from 'react'
import { ResetPasswordPage } from './ResetPasswordPage'
import { ResetPasswordSuccess } from './ResetPasswordSuccess'

export function ResetPasswordIndex() {
  const [step, setStep] = useState(1)
  const handleNavigation = () => setStep(step + 1)
  const forms = () => {
    switch (step) {
      case 1:
        return <ResetPasswordPage handleNavigation={handleNavigation} />
      case 2:
        return <ResetPasswordSuccess handleNavigation={handleNavigation} />
      default:
        return false
    }
  }

  return forms()
}
