import { useState } from 'react'
import { HeadingLarge } from 'styles/general-styles'
import { Link } from 'react-router-dom'
import { MdKeyboardArrowLeft } from 'react-icons/md'
import { ForgotPasswordForm } from '~/Forms/ForgotPassword'
import { RegisterNavigation } from '~/Navigation'
import { HelpModal } from '~/Modals/HelpModal'
import { BackgroundWrapper } from '~/BackgroundWrapper'
import { FloatingHelpIcon } from '~/FloatingHelpIcon'
import { ForgotPasswordStyled } from './styled'

const img = require('@img/reset-bg.jpeg')

export const ForgotPassword = () => {
  const [isModalOpen, setModalIsOpen] = useState(false)

  return (
    <ForgotPasswordStyled>
      <BackgroundWrapper className="bg-image" width="100%">
        <img
          src={img}
          alt="background"
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            zIndex: 0,
          }}
        />
      </BackgroundWrapper>

      <RegisterNavigation
        className="navigation"
        title={'Password Recovery'}
        backUrl="/login"
      />
      <div className="container">
        <div className="row d-flex justify-content-center">
          <div className="col-lg-7 col-md-9 col-12">
            <div className="form-card">
              <Link className="back-to-login" to={'/login'}>
                <MdKeyboardArrowLeft /> Back
              </Link>
              <div className="header-text">
                <HeadingLarge>Forgot Password?</HeadingLarge>
                <p>
                  Enter your email address below and we’ll send a reset password
                  link to your inbox.
                </p>
              </div>
              <ForgotPasswordForm />
            </div>
          </div>
        </div>
      </div>
      <HelpModal isOpen={isModalOpen} setModalIsOpen={setModalIsOpen} />
      <FloatingHelpIcon
        isModalOpen={isModalOpen}
        setModalIsOpen={setModalIsOpen}
      />
    </ForgotPasswordStyled>
  )
}
