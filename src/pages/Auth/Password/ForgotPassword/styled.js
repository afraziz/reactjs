import styled from 'styled-components'

export const ForgotPasswordStyled = styled.div`
  .bg-image {
    display: none;
    ${(props) => props.theme.mediaQueries.md} {
      display: block;
    }
  }
  .navigation {
    display: block;
    ${(props) => props.theme.mediaQueries.md} {
      display: none;
    }
  }
  .form-card {
    margin-bottom: 83px;
    margin-top: 32px;
    padding: 0;
    background: ${(props) => props.theme.colors.white};
    border-radius: 8px;
    .back-to-login {
      color: ${(props) => props.theme.colors.hbTextGreyLight};
      position: relative;

      display: none;
      svg {
        position: absolute;
        left: -25px;
        top: 0px;
        font-size: 20px;
      }
      &:hover {
        text-decoration: none;
      }
      ${(props) => props.theme.mediaQueries.md} {
        display: block;
      }
    }
    .logo {
      height: 68px;
      img {
        height: 100%;
        width: auto;
      }
    }
    .header-text {
      display: flex;
      flex-direction: column;
      text-align: center;
      padding: 0 6px;
      h1 {
        margin-top: 24px;
        margin-bottom: 8px;
        text-transform: uppercase;
      }
      p {
        font-size: 14px;
        font-weight: 500;
        color: ${(props) => props.theme.colors.hbTextGreyLight};
        line-height: 20px;
      }
      ${(props) => props.theme.mediaQueries.sm} {
        padding: 0 10px;
      }
      ${(props) => props.theme.mediaQueries.md} {
        padding: 0 20px;
      }
      ${(props) => props.theme.mediaQueries.lg} {
        padding: 0 77px;
      }
    }
    ${(props) => props.theme.mediaQueries.md} {
      padding: 30px 48px 48px 48px;
      margin-top: 25vh;
    }
  }
`
