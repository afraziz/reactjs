import styled from 'styled-components'

export const StayInTouchStyled = styled.div`
  .info-card {
    margin-top: 186px;
    margin-bottom: 186px;
    background: ${(props) => props.theme.colors.white};
    padding: 40px;
    border-radius: 8px;
    text-align: center;
    .call-icon {
      margin-bottom: 20px;
      padding: 10px;
      font-size: 44px;
      border-radius: 50%;
      width: 76px;
      height: 76px;
      background: ${(props) => props.theme.colors.hbGreyBackground};
      color: ${(props) => props.theme.colors.hbTextGreyDark};
      display: flex;
      justify-content: center;
      align-items: center;
    }
    h1 {
      margin-bottom: 20px;
    }
    p {
      margin-bottom: 24px;
    }
  }
`
