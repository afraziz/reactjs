import { ButtonOutline } from '~/Button'
import { HeadingLarge } from 'styles/general-styles'
import { IoIosCall } from 'react-icons/io'
import { BackgroundWrapper } from '~/BackgroundWrapper'
import { useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { resetDetails } from '@feat/registration/registrationSlice'
import { StayInTouchStyled } from './styled'

const backgroundImage = require('@img/stay-in-touch.png')

export const StayInTouch = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const handleClick = () => {
    dispatch({ type: resetDetails })
    navigate('/login')
  }
  return (
    <StayInTouchStyled>
      <BackgroundWrapper width="100%">
        <img
          src={backgroundImage}
          alt="background"
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            zIndex: 0,
          }}
        />
      </BackgroundWrapper>

      <div className="container">
        <div className="row d-flex justify-content-center">
          <div className="col-lg-5 col-md-6 col-12">
            <div className="info-card">
              <div className="d-flex justify-content-center">
                <div className="call-icon">
                  <IoIosCall />
                </div>
              </div>
              <HeadingLarge uppercase>We’ll be in touch shortly</HeadingLarge>
              <p>
                As we can’t match you details in our database, a representative
                from Harvey Beef will get in touch with you shortly to set you
                up with a new Supplier ID and account. We will typically get in
                touch within 24-48 hours.
              </p>
              <ButtonOutline onClick={handleClick} data-test-id="goToLogin">
                Go back to login
              </ButtonOutline>
            </div>
          </div>
        </div>
      </div>
    </StayInTouchStyled>
  )
}
