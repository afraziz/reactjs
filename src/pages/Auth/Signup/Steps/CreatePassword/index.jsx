import PropTypes from 'prop-types'
import { BackgroundWrapper } from '~/BackgroundWrapper'
import { Progressbar } from '~/Progressbar'
import {
  SignupStepBaseStyled,
  RegistrationFormWrapper,
} from '@page/Auth/Signup/styled'
import CreatePasswordForm from '~/Forms/Registration/CreatePasswordForm'
import { connect } from 'react-redux'
import { currentProgress } from '@feat/utils/helper'

const backgroundImage = require('@img/create-password-screen.png')

const CreatePassword = ({ totalSteps, progressStep, navigation }) => {
  const progress = currentProgress(totalSteps, progressStep)
  return (
    <SignupStepBaseStyled>
      <BackgroundWrapper className="image-section">
        <img
          src={backgroundImage}
          alt="background"
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            zIndex: 0,
          }}
        />
      </BackgroundWrapper>

      <div className="form-section">
        {navigation('Create Password')}
        <Progressbar progress={progress} />
        <RegistrationFormWrapper>
          <CreatePasswordForm />
        </RegistrationFormWrapper>
      </div>
    </SignupStepBaseStyled>
  )
}

CreatePassword.propTypes = {
  navigation: PropTypes.func,
  progressStep: PropTypes.any,
  totalSteps: PropTypes.any,
}
const mapStateToProps = (state) => ({
  progressStep: state.registration.progressStep,
  totalSteps: state.registration.totalSteps,
})
export default connect(mapStateToProps)(CreatePassword)
