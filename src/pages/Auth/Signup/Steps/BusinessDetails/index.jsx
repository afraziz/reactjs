import PropTypes from 'prop-types'
import {
  SignupStepBaseStyled,
  RegistrationFormWrapper,
} from '@page/Auth/Signup/styled'
import { BackgroundWrapper } from '~/BackgroundWrapper'
import BusinessDetailsForm from '~/Forms/Registration/BusinessDetailsForm'
import { Progressbar } from '~/Progressbar'
import { connect } from 'react-redux'
import { currentProgress } from '@feat/utils/helper'

const backgroundImage = require('@img/business-details.png')

const BusinessDetails = ({ totalSteps, progressStep, navigation }) => {
  const progress = currentProgress(totalSteps, progressStep)
  return (
    <SignupStepBaseStyled>
      <BackgroundWrapper className="image-section">
        <img
          src={backgroundImage}
          alt="background"
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            zIndex: 0,
          }}
        />
      </BackgroundWrapper>

      <div className="form-section">
        {navigation('Your Contact Details')}
        <Progressbar progress={progress} />
        <RegistrationFormWrapper>
          <BusinessDetailsForm />
        </RegistrationFormWrapper>
      </div>
    </SignupStepBaseStyled>
  )
}

BusinessDetails.propTypes = {
  navigation: PropTypes.func,
  progressStep: PropTypes.any,
  totalSteps: PropTypes.any,
}
const mapStateToProps = (state) => ({
  progressStep: state.registration.progressStep,
  totalSteps: state.registration.totalSteps,
})
export default connect(mapStateToProps)(BusinessDetails)
