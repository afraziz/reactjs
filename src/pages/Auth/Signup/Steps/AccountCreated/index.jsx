import { useState } from 'react'
import { Button } from '~/Button'
import { HeadingLarge } from 'styles/general-styles'
import { IoCheckmarkOutline } from 'react-icons/io5'
import { BackgroundWrapper } from '~/BackgroundWrapper'
import { RegisterNavigation } from '~/Navigation'
import { useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { resetDetails } from '@feat/registration/registrationSlice'
import { AccountCreatedStyled } from './styled'

const backgroundImage = require('@img/stay-in-touch.png')

export const AccountCreated = () => {
  const [isModalOpen, setModalIsOpen] = useState(false)
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const handleClick = () => {
    dispatch({ type: resetDetails })
    navigate('/login')
  }
  return (
    <AccountCreatedStyled>
      <BackgroundWrapper className="bg-image" width="100%">
        <img
          src={backgroundImage}
          alt="background"
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            zIndex: 0,
          }}
        />
      </BackgroundWrapper>
      <RegisterNavigation
        title={'Sign in'}
        hideBack
        showClose
        isModalOpen={isModalOpen}
        setModalIsOpen={setModalIsOpen}
        className="navigation"
      />

      <div className="container">
        <div className="row d-flex justify-content-center">
          <div className="col-lg-5 col-md-6 col-12">
            <div className="info-card">
              <div className="success-logo d-flex">
                <div className="call-icon">
                  <IoCheckmarkOutline />
                </div>
              </div>
              <HeadingLarge uppercase>
                Your Account has been created!
              </HeadingLarge>
              <p>
                You’re nearly there, to finish setting up your account we need
                to verify your email address. Check your email inbox for a
                confirmation link to finalize your account sign up.
              </p>
              <Button onClick={handleClick} data-test-id="goToLogin">
                Continue to login
              </Button>
            </div>
          </div>
        </div>
      </div>
    </AccountCreatedStyled>
  )
}

AccountCreated.propTypes = {}
