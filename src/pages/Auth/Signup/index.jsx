import PropTypes from 'prop-types'
import { useNavigate, useParams } from 'react-router-dom'
import { useState } from 'react'
import { FloatingHelpIcon } from '~/FloatingHelpIcon'
import { HelpModal } from '~/Modals/HelpModal'
import { RegisterNavigation } from '~/Navigation'
import { connect } from 'react-redux'
import SupplierID from './Steps/SupplierID'
import VerifyMobileNumber from './Steps/VerifyMobileNumber'
import { Email } from './Steps/Email'
import BusinessDetails from './Steps/BusinessDetails'
import CreatePassword from './Steps/CreatePassword'
import { SignupStyled } from './styled'
import { AccountCreated } from './Steps/AccountCreated'
import { StayInTouch } from './Steps/StayInTouch'

const Signup = ({ progressStep }) => {
  const navigate = useNavigate()
  const { type } = useParams()
  const [isModalOpen, setModalIsOpen] = useState(false)

  const navigation = (title) => {
    return (
      <RegisterNavigation
        title={title}
        isModalOpen={isModalOpen}
        setModalIsOpen={setModalIsOpen}
      />
    )
  }

  const forms = () => {
    if (type === 'supplier') {
      switch (progressStep) {
        case 1:
          return <SupplierID navigation={navigation} />
        case 2:
          return <BusinessDetails navigation={navigation} />
        case 3:
          return <VerifyMobileNumber navigation={navigation} />
        case 4:
          return <CreatePassword navigation={navigation} />
        case 5:
          return <AccountCreated />
        default:
          return <StayInTouch />
      }
    } else if (type === 'email') {
      switch (progressStep) {
        case 1:
          return <Email navigation={navigation} />
        case 2:
          return <VerifyMobileNumber navigation={navigation} />
        case 3:
          return <CreatePassword navigation={navigation} />
        case 4:
          return <AccountCreated />
        case 5:
          return <StayInTouch />
        default:
          return <StayInTouch />
      }
    } else {
      navigate('/signup/')
    }
    return false
  }

  return (
    <SignupStyled>
      {forms()}
      <FloatingHelpIcon
        isModalOpen={isModalOpen}
        setModalIsOpen={setModalIsOpen}
      />
      <HelpModal isOpen={isModalOpen} setModalIsOpen={setModalIsOpen} />
    </SignupStyled>
  )
}

Signup.propTypes = {
  progressStep: PropTypes.number,
}

const mapStateToProps = (state) => ({
  progressStep: state.registration.progressStep,
})
export default connect(mapStateToProps)(Signup)
