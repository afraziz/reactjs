import { createSlice } from '@reduxjs/toolkit'

const AuthTypes = {
  IS_LOGGED_IN: false,
  EMAIL: '',
  PASSWORD: '',
}

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    isLoggedIn: AuthTypes.IS_LOGGED_IN,
    email: AuthTypes.EMAIL,
    password: AuthTypes.PASSWORD,
  },
  reducers: {
    signin(state) {
      return {
        ...state,
        isLoggedIn: true,
      }
    },
    loginWithCredsOnly(state, action) {
      return {
        ...state,
        email: action.payload.email,
        password: action.payload.password,
      }
    },
    logout(state) {
      localStorage.removeItem('is_logged_in')
      return {
        ...state,
        isLoggedIn: false,
      }
    },
  },
})

export const { signin, loginWithCredsOnly, logout } = authSlice.actions

export default authSlice.reducer
