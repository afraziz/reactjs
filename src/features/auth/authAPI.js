import axios from 'axios'

const API_URL = process.env.REACT_APP_API_URL

export const checkEmail = async (email) => {
  return axios.post(`${API_URL}/check-email`, {
    email,
  })
}

export const checkPhone = async (phoneNumber) => {
  return axios.post(`${API_URL}/check-phone`, {
    phoneNumber,
  })
}

export const loginToServer = async (email, password) => {
  return axios.post(`${API_URL}/login`, {
    email,
    password,
  })
}

export const register = async (email, password, supplierId, phoneNumber) => {
  return axios.post(`${API_URL}/register`, {
    email,
    password,
    supplierId,
    phoneNumber,
  })
}

export const loginUser = (params) =>
  axios.post('auth/login', params).then(({ data }) => {
    localStorage.setItem(
      'authTokenScope',
      data?.two_factor ? 'twoFactorRequired' : 'full'
    )
    localStorage.setItem('authToken', data.token)
    return data
  })
