export const currentProgress = (totalSteps, progress) => {
  return `${(100 / totalSteps) * progress}%`
}
