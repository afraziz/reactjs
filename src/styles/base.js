import { createGlobalStyle } from 'styled-components'
import { theme } from 'theme'

export default createGlobalStyle`
    :root {
        font-family: ${theme.fonts.sans_serif};
        line-height: ${theme.lineHeights.base};
        font-weight: 400;
        font-size: 0.89rem;

        ${theme.mediaQueries.xxl} {
            font-size: 0.89rem;
        }
    }

    * {
        &,
        &:before,
        &:after {
            box-sizing: border-box;
        }
    }

    html {
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        text-rendering: optimizeLegibility;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }

    body {
        color: ${theme.colors.hbTextGrey};
        background-color: ${theme.colors.white};
    }

    ::selection {
        background: ${theme.colors.royalBlueLight};
        color: white;
    }

    ::-moz-selection {
        background: ${theme.colors.royalBlueLight};
        color: white;
    }

    .page-wrap {
        width: 100%;
        background-color: ${theme.colors.snow};
    }

    img {
        vertical-align: middle;
        max-width: 100%;
    }

    a {
        transition: ${theme.transitions.default};        
        font-weight: 500;
        font-size: 14px;
        text-decoration: none;
        color: ${(props) => props.theme.colors.hbPrimary};
        

        @include on-event() {
            color: ${theme.colors.hbPrimary};
            text-decoration: none;
        }
    }
    p{
        color: ${theme.colors.hbTextGreyLight};
        font-size: ${theme.fontSizes.sm};
        font-weight: ${theme.fontWeights.semiNormal};
        line-height: 20px;
    }

    strong {
        color: ${theme.colors.navy};
    }

    h1, h2, h3, h4, h5 {
        margin: 0;
    }
`
