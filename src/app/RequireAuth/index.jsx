import PropTypes from 'prop-types'
import { useEffect } from 'react'
import { useLocation, Navigate, Outlet } from 'react-router-dom'
import { connect } from 'react-redux'
import { signin } from '@feat/auth/authSlice'

const RequireAuth = ({ auth, signin }) => {
  const location = useLocation()
  const token = localStorage.getItem('is_logged_in') === 'true'

  useEffect(() => {
    if (token) {
      signin()
    }
  }, [signin, token])

  return !!auth?.isLoggedIn || token ? (
    <Outlet />
  ) : (
    <Navigate to="/login" state={{ from: location }} replace />
  )
}

RequireAuth.propTypes = {
  auth: PropTypes.shape({
    isLoggedIn: PropTypes.any,
  }),
  signin: PropTypes.any,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
})

export default connect(mapStateToProps, { signin })(RequireAuth)
