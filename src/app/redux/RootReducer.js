import { combineReducers } from 'redux'
import authReducer from '@feat/auth/authSlice'
import regReducer from '@feat/registration/registrationSlice'
import usersReducer from '@feat/user/usersSlice'

export const rootReducer = combineReducers({
  auth: authReducer,
  registration: regReducer,
  users: usersReducer,
})
