import { ReactComponent as BackBtn } from './back-btn.svg'
import { ReactComponent as HbLogo } from './HB_logo.svg'
import { ReactComponent as CloseIcon } from './close-icon.svg'
import { ReactComponent as CallIcon } from './call-Icon.svg'
import { ReactComponent as EmailIcon } from './email-icon.svg'
import { ReactComponent as SidebarLogo } from './sidebar-logo-icon.svg'
import { ReactComponent as CompanyLogo } from './harvest-company-logo.svg'
import { ReactComponent as AccountIcon } from './account.svg'
import { ReactComponent as AnalysisIcon } from './analysis.svg'
import { ReactComponent as BookingsIcon } from './bookings.svg'
import { ReactComponent as HomeIcon } from './home.svg'
import { ReactComponent as PaymentsIcon } from './payments.svg'
import { ReactComponent as NotificationIcon } from './notification.svg'
import { ReactComponent as BreadcrumbIcon } from './breadcrumb.svg'
import { ReactComponent as ProfileIcon } from './profile-icon.svg'
import { ReactComponent as SignoutIcon } from './signout-icon.svg'

export {
  BackBtn,
  HbLogo,
  CloseIcon,
  CallIcon,
  EmailIcon,
  SidebarLogo,
  CompanyLogo,
  AccountIcon,
  AnalysisIcon,
  BookingsIcon,
  HomeIcon,
  PaymentsIcon,
  NotificationIcon,
  BreadcrumbIcon,
  ProfileIcon,
  SignoutIcon,
}
