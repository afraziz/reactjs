import {
  AccountIcon,
  AnalysisIcon,
  BookingsIcon,
  HomeIcon,
  PaymentsIcon,
} from '@img/svg'
import { SidebarMenuWrapper } from './styled'
import { SidebarMenuItem } from './SidebarMenuItem'
import { HiUser, HiUserAdd } from 'react-icons/hi'

export const SidebarMenu = () => {
  return (
    <SidebarMenuWrapper>
      <SidebarMenuItem to="/user-list" title="Home" icon={<HomeIcon />} />
      <SidebarMenuItem to="/account-details" title="Account" icon={<AccountIcon />} />
    </SidebarMenuWrapper>
  )
}
