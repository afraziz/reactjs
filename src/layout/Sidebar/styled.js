import styled from 'styled-components'

export const SidebarWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
  z-index: 2;
  position: fixed;
  height: 83px;
  min-width: 100vh;
  max-width: 100%;
  width: 100%;
  left: 0px;
  bottom: 0px;

  background: #12284c;
  .sidebar-logo-top,
  .sidebar-logo-bottom {
    display: none;
  }
  ${(props) => props.theme.mediaQueries.md} {
    flex-direction: column;
    padding: 24px 0px;
    position: fixed;
    width: 112px;
    max-width: 112px;
    min-width: 112px;
    min-height: 100vh;
    max-height: 100%;
    height: 100%;
    left: 0px;
    top: 0px;

    background: #12284c;
    .sidebar-logo-top {
      display: block;
      padding: 30px 17px 50px 17px;
    }
    .sidebar-logo-bottom {
      display: block;
      padding: 30px 17px 30px 17px;
    }
  }
`

export const SidebarMenuWrapper = styled.div`
  display: flex;
  flex-direction: row;
  ${(props) => props.theme.mediaQueries.md} {
    display: flex;
    flex-direction: column;
    .menu-item:hover {
      background: ${(props) => props.theme.colors.hbPrimary};
      border-radius: 6px;
      cursor: pointer;
      color: ${(props) => props.theme.colors.white};
      svg {
        stroke: ${(props) => props.theme.colors.white};
        color: ${(props) => props.theme.colors.white};
        path {
          stroke: ${(props) => props.theme.colors.white};
        }
      }
    }
    .active-link {
      background: #3730a3;
      border-radius: 6px;
      cursor: pointer;
    }
  }
`

export const SidebarMenuItemWrapper = styled.div`
  text-align: center;
  padding: 10px;
  a {
    text-decoration: none;
  }
  .menu-title {
    color: #e0e7ff;
    font-weight: 500;
    font-size: 12px;
    line-height: 16px;
    margin-top: 10px;
  }
  ${(props) => props.theme.mediaQueries.md} {
    text-align: center;
    padding: 15px 21px 12px 21px;
    margin-bottom: 3px;
    a {
      text-decoration: none;
    }
    .menu-title {
      color: #e0e7ff;
      font-weight: 500;
      font-size: 12px;
      line-height: 16px;
      margin-top: 8px;
    }
  }
`
