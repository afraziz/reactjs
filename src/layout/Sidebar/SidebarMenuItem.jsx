import { Link, useLocation } from 'react-router-dom'
import PropTypes from 'prop-types'
import { SidebarMenuItemWrapper } from './styled'

export const SidebarMenuItem = ({ to, title, icon }) => {
  const { pathname } = useLocation()
  const currentUrl = pathname.split(/[?#]/)[0]
  const isActive = currentUrl === to

  return (
    <SidebarMenuItemWrapper
      className={`menu-item ${isActive ? 'active-link' : ''} `}
    >
      <Link to={to} className={{ active: true }}>
        <div className="menu-icon">{icon}</div>
        <div className="menu-title">{title}</div>
      </Link>
    </SidebarMenuItemWrapper>
  )
}

SidebarMenuItem.propTypes = {
  to: PropTypes.string,
  title: PropTypes.string,
  icon: PropTypes.any,
}
