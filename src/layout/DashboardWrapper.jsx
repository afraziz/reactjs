import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Sidebar } from './Sidebar'

const DashboardStyled = styled.div`
  .inner-container {
    padding-left: 150px;
    background: ${(props) => props.theme.colors.hbDashboardBackground};
  }
`
export const DashboardWrapper = ({ children }) => {
  return (
    <DashboardStyled>
      <Sidebar />
      <div className="inner-container">{children}</div>
    </DashboardStyled>
  )
}

DashboardWrapper.propTypes = {
  children: PropTypes.any,
}
