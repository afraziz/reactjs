import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import caution from '@img/caution.png'
import success from '@img/success.png'
import { signin } from '@feat/auth/authSlice'
import { setProgress } from '@feat/registration/registrationSlice'
import {
  HeadingSemiLarge,
  NotificationWrapper,
} from '../../styles/general-styles'

function Notification({ type, heading }) {
  let image = caution
  switch (type) {
    case 'success':
      image = success
      break
    default:
      image = caution
      break
  }

  return (
    <NotificationWrapper>
      <div className="container">
        <div className="row mb-4">
          <div className="col-sm-12">
            <img src={image} alt={type} />
          </div>
        </div>
        <div className="row mb-3">
          <div className="col-sm-12">
            <HeadingSemiLarge>{heading}</HeadingSemiLarge>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            {/* {primaryBtnDetails !== undefined ? (
              <Submit
                onClick={() => {
                  setProgress({
                    progressStep: primaryBtnDetails.goTo,
                  })
                }}
                text={primaryBtnDetails.text}
                alignSelf="center"
              />
            ) : (
              <></>
            )} */}
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            {/* {secondaryBtnDetails !== undefined ? (
              <Submit
                onClick={() => {
                  setProgress({
                    progressStep: secondaryBtnDetails.goTo,
                  })
                }}
                secondary
                text={secondaryBtnDetails.text}
                alignSelf="center"
              />
            ) : (
              <></>
            )} */}
          </div>
        </div>
      </div>
    </NotificationWrapper>
  )
}

Notification.propTypes = {
  heading: PropTypes.any,
  type: PropTypes.any,
}

const mapStateToProps = (state) => ({
  reg: state.registration,
})

export default connect(mapStateToProps, { setProgress, signin })(Notification)
