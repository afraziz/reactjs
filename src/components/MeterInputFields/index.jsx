import PropTypes from 'prop-types'
import { useState, useRef, useEffect } from 'react'
import { useField } from 'formik'
import { Error } from '~/FormFields/Error'
import { MeterInputFieldsWrapper } from './styled'

export const MeterInputFields = ({
  value,
  length,
  onComplete,
  name,
  handleChange,
  boxHeight,
}) => {
  const [code, setCode] = useState(
    value != null
      ? Array.from(value.toString()).map(Number)
      : [...Array(length)].map(() => '')
  )
  const inputs = useRef([])
  const meta = useField(name)[1]

  useEffect(() => {
    if (handleChange) {
      handleChange(name, code.join(''))
    }
  }, [code])

  const processInput = (e, slot) => {
    const num = e.target.value
    if (/[^0-9]/.test(num)) return
    const newCode = [...code]
    newCode[slot] = num
    setCode(newCode)
    if (newCode.every((num) => num !== '') && onComplete) {
      onComplete(newCode.join(''))
    }
  }

  const onKeyDown = (e, slot) => {
    if (/[^0-9]/.test(e.key) && e.keyCode !== 8) return
    if (slot !== length - 1 && e.keyCode !== 8 && code[slot]) {
      inputs.current[slot + 1].focus()
    }
    if (e.keyCode === 8 && !code[slot] && slot !== 0) {
      const newCode = [...code]
      newCode[slot] = ''
      setCode(newCode)
      inputs.current[slot - 1].focus()
    }
  }

  return (
    <>
      <MeterInputFieldsWrapper
        className="input-container"
        length={length}
        boxHeight={boxHeight}
      >
        {code.map((num, idx) => {
          return (
            <input
              className="meter-input-field"
              autoComplete="off"
              name={`${name}`}
              key={idx}
              type="text"
              inputMode="numeric"
              maxLength={1}
              value={num}
              autoFocus={!code[0].length && idx === 0}
              onChange={(e) => processInput(e, idx)}
              onKeyDown={(e) => {
                onKeyDown(e, idx)
              }}
              ref={(ref) => inputs.current.push(ref)}
            />
          )
        })}
      </MeterInputFieldsWrapper>
      <div style={{ position: 'relative' }}>
        <Error meta={meta} />
      </div>
    </>
  )
}

MeterInputFields.propTypes = {
  boxHeight: PropTypes.any,
  handleChange: PropTypes.func,
  length: PropTypes.number,
  name: PropTypes.any,
  onComplete: PropTypes.func,
  value: PropTypes.any,
}
