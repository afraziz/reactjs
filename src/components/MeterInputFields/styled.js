import styled from 'styled-components'

export const MeterInputFieldsWrapper = styled.div`
  display: flex;
  width: 100%;

  input:last-child {
    margin: 0px;
  }
  .meter-input-field {
    display: flex;
    width: 100%;
    outline: none;
    background: #ffffff;
    border: 1px solid #cfd1d8;
    box-sizing: border-box;
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1), 0px 1px 2px rgba(0, 0, 0, 0.06);
    border-radius: 3px;
    height: ${(props) => (props.boxHeight ? props.boxHeight : '105px')};
    margin-right: 12px;
    font-weight: bold;
    font-size: 48px;
    line-height: 100%;
    text-align: center;
    text-transform: uppercase;
    color: #12284c;
  }
  .meter-input-field:focus {
    border-color: #4f46e5;
    box-shadow: inset 0px 2px 2px -1px rgba(74, 74, 104, 0.1);
  }
`
