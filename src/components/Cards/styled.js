import styled from 'styled-components'

export const InfoCardStyled = styled.div`
  margin-top: 30px;
  border-radius: 8px;
  border: 0;
  box-shadow: 0px 1px 2px 0px #0000000f;
  box-shadow: 0px 1px 3px 0px #0000001a;
  overflow: hidden;
  .card-header {
    border-bottom: 1px solid
      ${(props) => props.theme.colors.hbDashboardBackground};
    background: ${(props) => props.theme.colors.white};
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 20px 24px;
    h4 {
      font-size: 18px;
      margin-bottom: 5px;
      color: ${(props) => props.theme.colors.hbTextBlue};
    }
    span {
      font-size: 14px;
      color: ${(props) => props.theme.colors.hbTextGreyLight};
    }
  }
  .card-body {
    padding: 0;
  }
  .card-footer {
    background: ${(props) => props.theme.colors.hbTableStriped};
    border: 0;
    padding: 24px;
    .label {
      margin-bottom: 5px;
      font-weight: 500;
      color: ${(props) => props.theme.colors.hbTextBlue};
    }
    .avatar {
      margin-right: 20px;
      height: 48px;
      width: 48px;
    }
    .btn {
      height: 34px;
    }
  }
`
