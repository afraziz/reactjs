import PropTypes from 'prop-types'
import { InfoCardStyled } from './styled'

export const InfoCard = ({ children, className }) => {
  return (
    <InfoCardStyled className={`card ${className}`}>{children}</InfoCardStyled>
  )
}

InfoCard.propTypes = {
  children: PropTypes.any,
  className: PropTypes.any,
}
