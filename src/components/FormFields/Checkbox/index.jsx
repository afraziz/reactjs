import PropTypes from 'prop-types'
import { useField } from 'formik'
import styled from 'styled-components/macro'

import { Error } from '~/FormFields/Error'

const CheckboxWrap = styled.div`
  height: 16px;
  margin-right: 8px;
  display: ${(props) => (props.display ? props.display : 'block')};
  color: ${(props) => props.theme.colors.hbTextGreyDark};
  .highlighted-label {
    color: #4f46e5;
    cursor: pointer;
    text-decoration: none;
  }
`

export const CheckboxStyled = styled.label`
  display: inline-block;
  align-items: center;
  position: relative;
  padding-left: 24px;
  cursor: pointer;
  user-select: none;
  font-size: ${(props) => props.theme.fontSizes.sm};
  font-weight: 500;

  input {
    position: absolute;
    opacity: 0;
    cursor: pointer;

    &::checked .checkmark {
      background-color: ${(props) => props.theme.colors.hbPrimary};
    }
  }

  .checkmark {
    position: absolute;
    top: 3px;
    left: 0;
    height: 16px;
    width: 16px;
    border-radius: 4px;
    border: 1px solid ${(props) => props.theme.colors.hbBorder};
    background-color: #eee;
    display: block;

    &::after {
      content: '';
      position: absolute;
      opacity: 0;
      left: 4px;
      top: 0px;
      width: 5px;
      height: 11px;
      border: solid white;
      border-width: 0 2px 2px 0;
      transform: rotate(45deg);
      transition: ${(props) => props.theme.transitions.default};
    }
  }

  &::hover input ~ .checkmark {
    background-color: #ccc;
  }

  input:checked ~ .checkmark {
    background-color: ${(props) => props.theme.colors.hbPrimary};
    border-color: ${(props) => props.theme.colors.hbPrimary};
  }

  input:checked ~ .checkmark::after {
    opacity: 1;
  }
`

export function Checkbox(props) {
  const { label, className, name, handleChange } = props
  const [meta] = useField(props)
  const onChange = (e) => {
    if (handleChange) {
      handleChange(name, e.target.checked)
    }
  }

  return (
    <>
      <CheckboxWrap className={className}>
        <CheckboxStyled className="container-checkbox">
          {label}
          <input
            type="checkbox"
            name={name}
            // checked={field.value}
            onChange={onChange}
          />
          <span className="checkmark" />
        </CheckboxStyled>
        {props.highlighted_label && (
          <a className="highlighted-label">&nbsp;{props.highlighted_label}</a>
        )}
      </CheckboxWrap>
      <div style={{ position: 'relative' }}>
        <Error meta={meta} />
      </div>
    </>
  )
}

Checkbox.propTypes = {
  children: PropTypes.any,
  className: PropTypes.any,
  handleChange: PropTypes.func,
  label: PropTypes.any,
  name: PropTypes.any,
  highlighted_label: PropTypes.any,
}
