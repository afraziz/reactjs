import PropTypes from 'prop-types'
import styled from 'styled-components/macro'
import { RiErrorWarningFill } from 'react-icons/ri'

export const ErrorStyled = styled.p`
  display: flex;
  align-items: center;
  color: ${(props) => props.theme.colors.hbError};
  font-weight: 500;
  margin: 0px !important;
  font-size: ${(props) => props.theme.fontSizes.sm};
  position: relative;
  top: 3px;
  left: ${(props) => (props.leftAlign ? '95px' : '0px')};
  right: ${(props) => (props.leftAlign ? 'auto' : '15px')};
  svg {
    margin-right: 6px;
  }
`

export function ErrorText({ text, ...props }) {
  return (
    <ErrorStyled {...props}>
      <RiErrorWarningFill icon="fas fa-info-circle" />
      {text}
    </ErrorStyled>
  )
}

ErrorText.propTypes = {
  text: PropTypes.any,
}

export function Error({ meta, text, leftAlign }) {
  if (text || (meta.touched && meta.error)) {
    return (
      <ErrorText
        text={text || (Array.isArray(meta.error) ? meta.error[0] : meta.error)}
        leftAlign={leftAlign}
      />
    )
  }
  return null
}

Error.propTypes = {
  leftAlign: PropTypes.any,
  meta: PropTypes.shape({
    error: PropTypes.any,
    touched: PropTypes.any,
  }),
  text: PropTypes.any,
}
