import PropTypes from 'prop-types'
import styled from 'styled-components/macro'

const ButtonStyled = styled.button`
  font-family: Cervo Neue Bold;
  outline: none;
  border: none;
  border-radius: 6px;
  color: ${(props) => props.theme.colors.white};
  background-color: ${(props) =>
    props.secondary
      ? props.theme.colors.blueSecondary
      : props.theme.colors.hbPrimary};
  align-self: flex-end;
  position: relative;
  top: 0;
  min-width: ${(props) => (props.refineResults ? '150px' : '')};
  padding: ${(props) => (props.refineResults ? '14px 27px' : '17px 27px')};
  width: ${(props) => (props.widthExpand ? '100%' : '190px')};
  margin-top: ${(props) => (props.marginTop ? props.marginTop : 0)};
  margin-right: ${(props) => (props.marginRight ? props.marginRight : 0)};
  float: ${(props) => (props.float ? props.float : 'none')};
  align-self: ${(props) => (props.alignSelf ? props.alignSelf : 'auto')};
  text-transform: ${(props) => (props.uppercase ? 'uppercase' : 'none')};
  font-weight: ${(props) => props.theme.fontWeights.bold};
  font-size: 20px;
  cursor: pointer;
  box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.05);
  &:hover {
    background: ${(props) => props.theme.colors.hbPrimaryHover};
    box-shadow: 0px 0px 12px 0px #0000000d inset;
  }
  &:disabled {
    background-color: ${(props) => props.theme.colors.hbDisabled};
    color: ${(props) => props.theme.colors.grayDark};
  }

  &:active,
  &:focus {
    outline: none;
  }
`

export function Submit({ type, text, isSubmitting, refineResults, ...props }) {
  return (
    <ButtonStyled
      type={type || 'submit'}
      disabled={isSubmitting}
      disablePointer={isSubmitting}
      disabledStyle={isSubmitting}
      refineResults={refineResults}
      {...props}
    >
      {isSubmitting
        ? props.submittingText || 'Submitting...'
        : text || 'Submit'}
    </ButtonStyled>
  )
}

Submit.propTypes = {
  isSubmitting: PropTypes.any,
  refineResults: PropTypes.any,
  text: PropTypes.string,
  type: PropTypes.string,
  submittingText: PropTypes.string,
}
