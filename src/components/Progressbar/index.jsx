import PropTypes from 'prop-types'
import { ProgressbarStyled } from './styled'

export const Progressbar = ({ children, ...props }) => {
  return (
    <ProgressbarStyled {...props}>
      <div className="progress">{children}</div>
    </ProgressbarStyled>
  )
}

Progressbar.propTypes = {
  children: PropTypes.any,
}
