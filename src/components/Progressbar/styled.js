import styled from 'styled-components'

export const ProgressbarStyled = styled.div`
  width: 100%;
  height: ${(props) => (props.height ? props.height : '5px')};
  background: ${(props) => props.theme.colors.hbGreyBackground};
  .progress {
    height: 100%;
    width: ${(props) => (props.progress ? props.progress : '0')};
    background: ${(props) => props.theme.colors.hbPrimary};
    border-radius: 0;
  }
`
