import PropTypes from 'prop-types'
import { MdKeyboardArrowLeft } from 'react-icons/md'
import { useNavigate } from 'react-router-dom'
import { CallIcon } from '../../images/svg'
import {
  RegisterBackButton,
  RegisterNavigationStyled,
  RegisterTitle,
} from './styled'

export const ForgotPasswordNavigation = ({ hideBack, title, className }) => {
  const navigate = useNavigate()
  return (
    <RegisterNavigationStyled className={className} width="50%">
      {!hideBack && (
        <RegisterBackButton
          className="btn btn-default"
          onClick={() => {
            navigate('/login')
          }}
        >
          <MdKeyboardArrowLeft />
        </RegisterBackButton>
      )}
      <RegisterTitle>{title}</RegisterTitle>
      <a className="help-icon" href="tel:+61 8 9729 0000">
        <CallIcon />
      </a>
    </RegisterNavigationStyled>
  )
}

ForgotPasswordNavigation.propTypes = {
  title: PropTypes.any,
  hideBack: PropTypes.bool,
  className: PropTypes.string,
}
