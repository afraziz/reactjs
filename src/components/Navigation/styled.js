import styled from 'styled-components'

export const RegisterNavigationStyled = styled.div`
  position: relative;
  width: 100%;
  padding: 22px;
  border-bottom: 1px solid ${(props) => props.theme.colors.hbGreyBackground};
  background: ${(props) => props.theme.colors.white};
  color: ${(props) => props.theme.colors.hbTextBlue};
  font-size: 14px;
  .help-icon {
    position: absolute;
    top: 26%;
    right: 20px;
    font-size: 22px;
    margin-bottom: 2px;
  }
`

export const RegisterBackButton = styled.button`
  position: absolute;
  top: 26%;
  left: 0;
  font-weight: ${(props) => props.theme.fontWeights.semiNormal};
  padding-left: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  svg {
    margin-right: 20px;
    font-size: 24px;
    margin-bottom: 2px;
  }
`
export const RegisterCloseButton = styled.button`
  position: absolute;
  top: 26%;
  left: 0;
  font-weight: ${(props) => props.theme.fontWeights.semiNormal};
  padding-left: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  svg {
    margin-right: 20px;
    font-size: 24px;
    margin-bottom: 2px;
  }
`

export const RegisterTitle = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: ${(props) => props.theme.fontWeights.semiBold};
`
