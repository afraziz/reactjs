import PropTypes from 'prop-types'
import { MdKeyboardArrowLeft } from 'react-icons/md'
import { connect } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { updateProgress } from '@feat/registration/registrationActions'
import { CloseIcon, CallIcon } from '../../images/svg'
import {
  RegisterBackButton,
  RegisterCloseButton,
  RegisterNavigationStyled,
  RegisterTitle,
} from './styled'

const RegisterNavigationComponent = ({
  hideBack,
  backUrl,
  title,
  className,
  showClose,
  updateProgress,
  progressStep,
}) => {
  const navigate = useNavigate()
  return (
    <RegisterNavigationStyled className={className} width="50%">
      {!hideBack && (
        <RegisterBackButton
          className="btn btn-default"
          onClick={() => {
            if (updateProgress) {
              if (progressStep === 1) {
                navigate('/login')
              } else {
                updateProgress(progressStep, true)
              }
            }
            if (backUrl) {
              navigate(backUrl)
            }
          }}
        >
          <MdKeyboardArrowLeft />
          <span className="d-lg-block d-md-block d-none">Back</span>
        </RegisterBackButton>
      )}
      {showClose && (
        <RegisterCloseButton className="btn btn-default" onClick={() => {}}>
          <CloseIcon />
        </RegisterCloseButton>
      )}
      <RegisterTitle>{title}</RegisterTitle>
      <a
        className="help-icon d-sm-block d-lg-none d-md-none"
        href="tel:+61 8 9729 0000"
      >
        <CallIcon />
      </a>
    </RegisterNavigationStyled>
  )
}

RegisterNavigationComponent.propTypes = {
  backUrl: PropTypes.string,
  className: PropTypes.string,
  hideBack: PropTypes.bool,
  isModalOpen: PropTypes.any,
  progressStep: PropTypes.any,
  setModalIsOpen: PropTypes.func,
  showClose: PropTypes.bool,
  title: PropTypes.any,
  updateProgress: PropTypes.func,
}

const mapStateToProps = (state) => ({
  progressStep: state.registration.progressStep,
})

export const RegisterNavigation = connect(mapStateToProps, { updateProgress })(
  RegisterNavigationComponent
)
