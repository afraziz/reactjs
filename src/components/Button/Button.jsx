import PropTypes from 'prop-types'
import { useNavigate } from 'react-router-dom'
import { ButtonStyled } from './styled'

export const Button = (props) => {
  const navigate = useNavigate()
  const { children, classes, onClick, url } = props
  const handleOnClick = () => {
    if (onClick) {
      onClick()
    }
    if (url) {
      navigate(url)
    }
  }

  return (
    <ButtonStyled
      className={classes ? `${classes} btn` : 'btn'}
      onClick={handleOnClick}
      {...props}
    >
      {children}
    </ButtonStyled>
  )
}

Button.propTypes = {
  children: PropTypes.any,
  classes: PropTypes.string,
  onClick: PropTypes.func,
  url: PropTypes.string,
}
