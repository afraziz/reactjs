import PropTypes from 'prop-types'
import { MdEmail } from 'react-icons/md'
import { EmailIcon } from '@img/svg/index'
import { BaseModal } from '~/Modals/BaseModal'
import { Button, ButtonOutline } from '~/Button'

import { PasswordResetModalStyled } from './styled'

export const PasswordResetModal = ({ isOpen, setModalIsOpen }) => {
  return (
    <PasswordResetModalStyled>
      <BaseModal
        hideClose
        isOpen={isOpen}
        width="380px !important"
        onRequestClose={() => {
          setModalIsOpen(false)
        }}
      >
        <div className="py-3">
          <div className="d-flex justify-content-center">
            <div className="email-logo-icon">
              <MdEmail color="#065F46" />
            </div>
          </div>
          <h3 className="text-center">
            Your password reset link is on it’s way.
          </h3>
          <p className="text-center">
            If an account registered with that email address exists, you’ll soon
            get an email with your password reset link.
          </p>
          <div className="footer">
            <ButtonOutline width="50%" url="/login">
              Go back to login
            </ButtonOutline>
            <Button onClick={() => {}} width="50%">
              <EmailIcon /> <span className="email-icon">Open Email</span>
            </Button>
          </div>
        </div>
      </BaseModal>
    </PasswordResetModalStyled>
  )
}

PasswordResetModal.propTypes = {
  isOpen: PropTypes.bool,
  setModalIsOpen: PropTypes.func,
}
