import styled from 'styled-components'

export const PasswordResetModalStyled = styled.div`
  font-size: 16px;
  .email-icon {
    padding-left: 10px;
  }
`
