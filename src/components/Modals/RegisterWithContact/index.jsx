import PropTypes from 'prop-types'
import { BaseModal } from '~/Modals/BaseModal'
import { Button } from '~/Button'
import { ButtonOutline } from '~/Button/ButtonOutline'
import { RiQuestionLine } from 'react-icons/ri'

import { RegisterWithContactStyled } from './styled'

export const RegisterWithContact = (props) => {
  const { isOpen, setModalIsOpen } = props
  return (
    <RegisterWithContactStyled>
      <BaseModal
        hideClose
        isOpen={isOpen}
        width="510px"
        onRequestClose={() => {
          setModalIsOpen(false)
        }}
      >
        <div className="question-icon">
          <RiQuestionLine color="#92400E" />
        </div>
        <h3>DON’T HAVE A Supplier ID?</h3>
        <p>
          If you’ve previously been one of our partners you’ll find this ID
          number on your registration paperwork or a recent Harvey Beef invoice.
          Other places you might find your supplier ID:
        </p>
        <ul>
          <li>1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
        </ul>
        <p>
          If you would like to sign up as a new supplier or have lost your
          supplier ID please proceed below.
        </p>
        <div className="footer">
          <Button
            onClick={() => {
              setModalIsOpen(false)
            }}
            width="40%"
          >
            I found it
          </Button>

          <ButtonOutline width="60%" url="/signup/email">
            Proceed without Supplier ID
          </ButtonOutline>
        </div>
      </BaseModal>
    </RegisterWithContactStyled>
  )
}

RegisterWithContact.propTypes = {
  isOpen: PropTypes.any,
  setModalIsOpen: PropTypes.func,
}
