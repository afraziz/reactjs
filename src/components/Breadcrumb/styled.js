import styled from 'styled-components'

export const BreadcrumbStyled = styled.div`
  height: 45px;
  background: ${(props) => props.theme.colors.white};
  margin: 0;
  padding: 0;
  padding: 0 34px;
  border-bottom: 1px solid #e5e7eb;
  display: flex;
  align-items: center;
  .home-icon {
    font-size: 20px;
    color: ${(props) => props.theme.colors.hbIconGrey};
    margin-right: 18px;
  }

  .page-title {
    margin-left: 17px;
    font-size: 14px;
    color: ${(props) => props.theme.colors.hbTextGreyLight};
    font-weight: 500;
  }
`
