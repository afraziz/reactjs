import PropTypes from 'prop-types'
import { useState } from 'react'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'
import { PasswordResetModal } from '~/Modals/PasswordResetModal'
import { Submit, TextInput } from '../../FormFields'

export const ForgotPasswordForm = () => {
  const [isModalOpen, setModalIsOpen] = useState(false)

  const handleSubmit = (values) => {
    if (values) {
      setModalIsOpen(true)
    }
  }

  const validationSchema = Yup.object({
    email: Yup.string().email().required().label('Email'),
  })

  const initialValues = {
    email: '',
  }
  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(props) => {
          const { isSubmitting } = props

          return (
            <Form>
              <div className="row">
                <div className="col-sm-12">
                  <TextInput label="Email address" name="email" type="email" />
                </div>
              </div>

              <div className="row">
                <div className="col-sm-12">
                  <Submit
                    type="submit"
                    isSubmitting={isSubmitting}
                    marginTop="26px"
                    text="Sign in"
                    uppercase
                    submittingText="Processing..."
                    widthExpand
                  />
                </div>
              </div>
            </Form>
          )
        }}
      </Formik>
      <PasswordResetModal
        isOpen={isModalOpen}
        setModalIsOpen={setModalIsOpen}
      />
    </>
  )
}

ForgotPasswordForm.propTypes = {
  signin: PropTypes.func,
  isSubmitting: PropTypes.func,
}
