import PropTypes from 'prop-types'
import { useState } from 'react'
import { MeterInputFields } from '~/MeterInputFields'
import { Form, Formik } from 'formik'
import { HeadingLarge } from 'styles/general-styles'
import { Submit } from '~/FormFields'
import * as Yup from 'yup'
import { RegisterWithContact } from '~/Modals'
import { connect } from 'react-redux'
import {
  updateProgress,
  verifySupplierId,
} from '@feat/registration/registrationActions'
import { toast } from 'react-toastify'

const SupplierIDForm = ({ updateProgress, registration, verifySupplierId }) => {
  const [isModalOpen, setModalIsOpen] = useState(false)

  const handleSubmit = async (values) => {
    const res = await verifySupplierId(values)
    if (res.data.success === true) {
      updateProgress(registration.progressStep)
    } else {
      toast.error(res.data.message)
    }
  }
  const validationSchema = Yup.object({
    supplier_code: Yup.string()
      .required('Supplier ID is required')
      .matches(/^[0-9]{6}$/, 'Must be exactly 6 characters'),
  })

  const initialValues = {
    supplier_code: registration.supplierCode,
  }
  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {(props) => {
          const { setFieldValue, isSubmitting } = props
          return (
            <Form>
              <h4>Step 1</h4>
              <HeadingLarge uppercase>
                Sign up with your Supplier Number
              </HeadingLarge>
              <p>
                You’ll find this ID code on your registration paperwork or a
                recent Harvey Beef invoice.
              </p>
              <div className="supplier-id-container">
                <MeterInputFields
                  length={6}
                  value={
                    registration.supplierCode ? registration.supplierCode : null
                  }
                  handleChange={setFieldValue}
                  name="supplier_code"
                />
              </div>
              <div className="row">
                <div className="col-sm-12">
                  <Submit
                    type="submit"
                    marginTop="26px"
                    text="Continue"
                    uppercase
                    submittingText="Processing..."
                    widthExpand
                    isSubmitting={isSubmitting}
                  />
                </div>
                <div className="col-12">
                  <p className="signup mb-0">
                    Don’t have a Supplier ID? &nbsp;
                    <span
                      className="click-here-link"
                      role={'button'}
                      onClick={() => {
                        setModalIsOpen(true)
                      }}
                    >
                      Click here
                    </span>
                  </p>
                </div>
              </div>
            </Form>
          )
        }}
      </Formik>
      <RegisterWithContact
        isOpen={isModalOpen}
        setModalIsOpen={setModalIsOpen}
      />
    </>
  )
}

SupplierIDForm.propTypes = {
  isSubmitting: PropTypes.bool,
  registration: PropTypes.shape({
    progressStep: PropTypes.any,
    supplierCode: PropTypes.any,
    supplier_code: PropTypes.any,
  }),
  setFieldValue: PropTypes.func,
  updateProgress: PropTypes.func,
  verifySupplierId: PropTypes.func,
}

const mapStateToProps = (state) => ({
  registration: state.registration,
})

export default connect(mapStateToProps, { verifySupplierId, updateProgress })(
  SupplierIDForm
)
