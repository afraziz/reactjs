import PropTypes from 'prop-types'
import { useState } from 'react'
import { Form, Formik } from 'formik'
import { HeadingLarge } from 'styles/general-styles'
import { Submit, TextInput } from '~/FormFields'
import { HelpModal } from '~/Modals/HelpModal'
import * as Yup from 'yup'
import {
  updateProgress,
  register,
} from '@feat/registration/registrationActions'
import { connect } from 'react-redux'
import { toast } from 'react-toastify'

const ContactDetailsForm = ({ registration, updateProgress, register }) => {
  const [isModalOpen, setModalIsOpen] = useState(false)
  const handleSubmit = async (values) => {
    const formData = values
    formData.supplier_code = registration.supplierId

    const res = await register(values)

    if (res.success === true) {
      if (res.meta.access_token == null) {
        updateProgress(registration.progressStep + 5)
      } else {
        updateProgress(registration.progressStep)
      }
    } else if (res?.errors?.supplier_code) {
      updateProgress(registration.progressStep - 1)
      toast.error(res.errors.supplier_code)
    } else if (res.errors.email) {
      toast.error(res.errors.email[0])
    } else {
      toast.error(res.message)
    }
  }

  const validationSchema = Yup.object({
    first_name: Yup.string().required('First Name is required'),
    last_name: Yup.string().required('Last Name is required'),
    email: Yup.string().email().required('Email is required'),
    mobile: Yup.string()
      .required('Mobile Number is Required')
      .matches(
        /^(?=.*)((?:\+?61) ?(?:\((?=.*\)))?([2-47-8])\)?|(?:\((?=.*\)))?([0-1][2-47-8])\)?) ?-?(?=.*)((\d{1} ?-?\d{3}$)|(00 ?-?\d{4} ?-?\d{4}$)|( ?-?\d{4} ?-?\d{4}$)|(\d{2} ?-?\d{3} ?-?\d{3}$))/,
        'Invalid Phone No.'
      ),
  })

  const initialValues = {
    first_name: '',
    last_name: '',
    email: '',
    mobile: '',
  }

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting }) => {
          return (
            <Form>
              <h4>Step 1</h4>
              <HeadingLarge uppercase>
                SIGN UP BY ENTERING YOUR CONTACT DETAILS
              </HeadingLarge>
              <p>
                Don’t have a supplier number? Get started with Harvey Beef’s
                Supplier Portal by by entering your details below.
              </p>
              <div className="row">
                <div className="col-lg-6 col-md-6 col-12">
                  <TextInput
                    label="First Name"
                    required={false}
                    name="first_name"
                    type="text"
                  />
                </div>
                <div className="col-lg-6 col-md-6 col-12">
                  <TextInput
                    label="Last Name"
                    required={false}
                    name="last_name"
                    type="text"
                  />
                </div>
                <div className="col-12">
                  <TextInput
                    label="Email"
                    required={false}
                    name="email"
                    type="email"
                  />
                </div>
                <div className="col-12">
                  <TextInput
                    label="Mobile Number"
                    required={false}
                    name="mobile"
                    type="tel"
                  />
                </div>
                <div className="col-12">
                  <Submit
                    type="submit"
                    marginTop="26px"
                    text="Continue"
                    uppercase
                    submittingText="Processing..."
                    widthExpand
                    isSubmitting={isSubmitting}
                  />
                </div>
                <div className="col-12">
                  <p className="signup mb-0">
                    Having trouble? &nbsp;
                    <span
                      className="click-here-link"
                      role={'button'}
                      onClick={() => {
                        setModalIsOpen(true)
                      }}
                    >
                      Talk to our team
                    </span>
                  </p>
                </div>
              </div>
            </Form>
          )
        }}
      </Formik>
      <HelpModal isOpen={isModalOpen} setModalIsOpen={setModalIsOpen} />
    </>
  )
}

ContactDetailsForm.propTypes = {
  isSubmitting: PropTypes.func,
  register: PropTypes.func,
  registration: PropTypes.shape({
    progressStep: PropTypes.number,
    supplierId: PropTypes.any,
  }),
  updateProgress: PropTypes.func,
}

const mapStateToProps = (state) => ({
  registration: state.registration,
})
export default connect(mapStateToProps, {
  updateProgress,
  register,
})(ContactDetailsForm)
