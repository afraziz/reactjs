import PropTypes from 'prop-types'
import { TableStyled } from './styled'

export const Table = ({ children, className }) => {
  return <TableStyled className={`table ${className}`}>{children}</TableStyled>
}

Table.propTypes = {
  children: PropTypes.any,
  className: PropTypes.any,
}
