import styled from 'styled-components'

export const TableStyled = styled.table`
  font-weight: 500;
  margin-bottom: 0;
  color: ${(props) => props.theme.colors.hbTextGreyLight};
  tr,
  td {
    border: 0;
    padding: 20px 24px;
  }
  th{
    border: 0;
    padding: 20px 24px;
  }

  &.table-striped tbody tr:nth-of-type(odd) {
    background: ${(props) => props.theme.colors.hbTableStriped};
  }
`
